;;; -*- no-byte-compile: t -*-
(define-package "verilog-mode" "2020.6.27.14326051" "major mode for editing verilog source in Emacs" 'nil :keywords '("languages") :authors '(("Michael McNamara" . "mac@verilog.com") ("Wilson Snyder" . "wsnyder@wsnyder.org")) :maintainer '("Michael McNamara" . "mac@verilog.com") :url "https://www.veripool.org")
