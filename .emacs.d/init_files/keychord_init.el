
;;********************
;; Key chord
;;********************
    (load "key-chord.el")
    (require 'key-chord)
    (key-chord-mode 1)

;; Max time delay between two key presses to be considered a key chord
    (setq key-chord-two-keys-delay 0.1) ; default 0.1

;; Max time delay between two presses of the same key to be considered a key chord.
;; Should normally be a little longer than `key-chord-two-keys-delay'.
    (setq key-chord-one-key-delay 0.2) ; default 0.2

    (defun switch-to-previous-buffer ()
    	"Switch to previously open buffer.
    	Repeated invocations toggle between the two most recently open buffers."
    	(interactive)
    	(switch-to-buffer (other-buffer (current-buffer) 1)))


;;****************
;; Bookmarks
;;****************

;; Define bookmarks function

;; Emacs
  (defun bookmark_keychord ()
    	"Switch to keychord init file"
    	(interactive)
    	(bookmark-jump "bookmark_keychord" ))
  (defun bookmark_setup ()
    	"Switch to setup  init file"
    	(interactive)
    	(bookmark-jump "bookmark_setup" ))
  (defun bookmark_emacs ()
    	"Switch to .emacs file"
    	(interactive)
    	(bookmark-jump "bookmark_emacs" ))

;; Various
  (defun bookmark_windows ()
    	"Switch to windows bookmark notes"
    	(interactive)
    	(bookmark-jump "notes_windows" ))
  (defun bookmark_linux ()
    	"Switch to linux bookmark notes"
    	(interactive)
    	(bookmark-jump "notes_linux" ))

;;*********************************************************
;; Bindings
;;*********************************************************

;; diff
    (key-chord-define-global "dc" 'ediff-buffers)
    (key-chord-define-global "cb" 'diff-region-tag-selected-as-a)
    (key-chord-define-global "cv" 'diff-region-compare-with-b)

;; buffers
    (key-chord-define-global "jj" 'switch-to-prev-buffer)
    (key-chord-define-global "hh" 'switch-to-next-buffer)
    (key-chord-define-global "qw" 'revert-buffer)
    (key-chord-define-global "kk" 'kill-buffer)
    (key-chord-define-global "ww" 'other-window)

    (key-chord-define-global "m4" 'buf-move-right)
    (key-chord-define-global "m1" 'buf-move-left)
    (key-chord-define-global "m2" 'buf-move-down)
    (key-chord-define-global "m3" 'buf-move-up)

    (key-chord-define-global "s3" 'slide-buffer-up)
    (key-chord-define-global "s2" 'slide-buffer-down)
    (key-chord-define-global "s1" 'slide-buffer-left)
    (key-chord-define-global "s4" 'slide-buffer-right)

    (key-chord-define-global "c1" 'clone-indirect-buffer-other-window)

;; Frames
    (key-chord-define-global "f1" 'other-frame)

;; highlight
    (key-chord-define-global "hl" 'hl-highlight-thingatpt-local)
    (key-chord-define-global "h8" 'hl-unhighlight-all-local)
    (key-chord-define-global "hx" 'highlight-regexp)
    (key-chord-define-global "h9" 'hl-paren-mode)
    (key-chord-define-global "h4" 'hl-find-next-thing)
    (key-chord-define-global "h1" 'hl-find-prev-thing)
    (key-chord-define-global "h2" 'hl-save-highlights)
    (key-chord-define-global "h3" 'hl-restore-highlights)

;; magit
    (key-chord-define-global "ms" 'magit-status)
    (key-chord-define-global "ml" 'magit-log-all-branches)
    (key-chord-define-global "m;" 'magit-log-current)
    (key-chord-define-global "mf" 'magit-log-buffer-file)
    (key-chord-define-global "cf" 'magit-checkout-file)
    (key-chord-define-global "bm" 'magit-blame)
    (key-chord-define-global "mc" 'magit-file-checkout)

;; winner mode
    (key-chord-define-global "w1" 'winner-undo)
    (key-chord-define-global "w4" 'winner-redo)

;;*********************************************************
;; bookmarks
;;*********************************************************
    (key-chord-define-global "bs" 'bookmark-set)
    (key-chord-define-global "bj" 'bookmark-jump)
    (key-chord-define-global "bh" 'list-bookmarks)
;;  (key-chord-define-global "bk" 'bookmark_keychord)

;;*********************************************************
;; various
;;*********************************************************

;; Replace
    (key-chord-define-global "xn" 'query-replace)
    (key-chord-define-global "xx" 'query-replace-regexp)
    (key-chord-define-global "xb" 're-builder)
;; Search
    (key-chord-define-global "sx" 'search-forward-regexp)
    (key-chord-define-global "sw" 'search-backward-regexp)
;; Registers
    (key-chord-define-global "rc" 'copy-to-register)
    (key-chord-define-global "rv" 'insert-register)
    (key-chord-define-global "rl" 'list-registers)
    (key-chord-define-global "wm" 'whitespace-mode)
    (key-chord-define-global "yv" 'ivy-mode)
    (key-chord-define-global "q1" 'undo)
    (key-chord-define-global "ld" 'load-file)
    (key-chord-define-global "wk" 'delete-trailing-whitespace)
    (key-chord-define-global "pk" 'align-regexp)
    (key-chord-define-global "bk" 'browse-kill-ring)

;; Macros
    ;; Save the last macro
       (key-chord-define-global "mk" 'kmacro-name-last-macro)
    ;; insert a macro in the buffer. This is use to add a macro in the .emacs file
       (key-chord-define-global "mr" 'insert-kbd-macro)

;; align code in verilog. The following keychord are macro defined in the .emacs file
    (key-chord-define-global "a," 'pk_coma)
    (key-chord-define-global "a;" 'pk_semicol)
    (key-chord-define-global "a[" 'pk_less)
    (key-chord-define-global "a=" 'pk_equal)
    (key-chord-define-global "a9" 'pk_open_parenthesis)
    (key-chord-define-global "A:" 'pk_col)
    (key-chord-define-global "[;" 'pk_less_semi)
    (key-chord-define-global ":L" 'pk_col_semicol)
    (key-chord-define-global "a/" 'pk_slash)
