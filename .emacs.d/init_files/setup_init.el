;;******************************************************************
;;                    config file for emacs
;;******************************************************************

;;To remove splash screen
(setq inhibit-startup-screen t)


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "black" :foreground "grey" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 99 :width normal :foundry "unknown" :family "DejaVu Sans Mono")))))

;; initial size of emacs
(split-window-horizontally)

(setq initial-frame-alist
	  '(
		(tool-bar-lines . 0)
		(width . 230) ; chars
		(height . 53) ; lines
		(left . 0)
		(top . 0)))

;; To remove tabs and teat them as spaces
 (setq-default indent-tabs-mode nil)

;; Set tab to 4 spaces
(setq-default tab-width 4)
(setq indent-line-function 'insert-tab)

;; scrolling options
(setq mouse-wheel-scroll-amount '(10 ((shift) . 1)))	;; 10 by 10 lines at a time
(setq mouse-wheel-progressive-speed nil)				;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't)						;; scroll window under mouse
(setq scroll-step 1)									;; keyboard scroll one line at a time

;;change mouse color
(set-mouse-color "white")

;; Trunkate long line at
(set-default 'truncate-lines t)

;;set time format
(setq display-time-format "%d/%m/%Y %I:%M")

;; display time by default
(display-time)

(setq load-path (cons (expand-file-name "~/.emacs.d/lisp") load-path))

;;***********************************************
;;  To put he name of the file in the up bar
;;***********************************************

(setq frame-title-format
      '("" invocation-name ": "
        (:eval
         (if buffer-file-name
             (abbreviate-file-name buffer-file-name)
           "%b")) " %I"))

;;********************
;; Verilog
;;********************

(autoload 'verilog-mode "verilog-mode" "Verilog mode" t )
(add-to-list 'auto-mode-alist '("\\.[ds]?va?h?\\'" . verilog-mode))


;;********************
;; Require packages
;;********************

;;(require 'buffer-move)

;;********************
;; registers
;;********************

;;(require 'iregister)
;;(global-set-key (kbd "M-n") 'iregister-jump-to-next-marker)
;;(global-set-key (kbd "M-p") 'iregister-jump-to-previous-marker)
;;(global-set-key (kbd "M-u") 'iregister-point-or-text-to-register)
;;(global-set-key (kbd "C-c") 'undefined)
;;(global-set-key (kbd "C-c") 'kill-ring-save)
;;(global-set-key (kbd "C-v") 'yank)

;;********************
;; auto-complete-mode
;;********************

(global-auto-complete-mode 1)
;;********************
;; undo-tree-mode
;;********************
;;To activate the undo tree mode in all buffers
(global-undo-tree-mode 1)

;;********************
;; Ivy
;;********************
;; Thi mode allow to see a list of opions in the minibuffer.
;; Need to check how to be hable of keeping the commands history with this mode
;; spetially when using M-x
;; To install the package at ST, it is necessary to first install the avy package
;; Then you need to compile the swiper in the directory swiper-master
;;    - make compile
;; Finally you need to put the following line

(add-to-list 'load-path "~/.emacs.d/elpa/swiper-master/")
(load "ivy")
(require 'ivy)
(ivy-mode 1)


;;********************
;; Browse-kill-ring
;;********************

(require 'browse-kill-ring)
(global-set-key (kbd "C-c k") 'browse-kill-ring)

;; Trunkate long line at
    (set-default 'truncate-lines t)

;;set time format
    (setq display-time-format "%d/%m/%Y %I:%M")
;; display time by default
    (display-time)

    (setq load-path (cons (expand-file-name "~/.emacs.d/elpa") load-path))

;;***********************************************
;; VHDL mode
;;***********************************************
(autoload 'vhdl-mode "vhdl-mode" "VHDL Mode" t)
(setq auto-mode-alist (cons '("\\.vhdl?\\'" . vhdl-mode) auto-mode-alist))

;;***********************************************
;;  magit package
;;***********************************************

(require 'package)
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa-stable" . "/home/riosd/.emacs.d/melpa-master/packages/") t)
;; (setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
;;                      ("marmalade" . "http://marmalade-repo.org/packages/")
;;                      ("melpa" . "http://melpa.org/packages/")))

(package-initialize)
;;	(setq package-archives '(("local-melpa" . "/home/riosd/.emacs.d/melpa-master/packages/")))


;;************************************************************************************
;; Activate winner mode to restaure workplace configuration
;;************************************************************************************
	(when (fboundp 'winner-mode)
      (winner-mode 1))
;; (require 'winner-mode)
;;(winer-mode 1)
;;************************************************************************************
;; session manager
;;************************************************************************************
;(add-to-list 'load-path "~/.emacs.d/elpa/session/")
;   (require 'session)
;   (add-hook 'after-init-hook 'session-initialize)


;;************************************************************************************
;; org mode
;;************************************************************************************

;; Enable transient mark mode
;;(transient-mark-mode 1)

;;;;Org mode configuration
;; Enable Org mode
;;(require 'org)
;; Make Org mode work with files ending in .org
;; (add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
;; The above is the default in recent emacsen

;;************************************************************************************
;; bookmarks
;;************************************************************************************

(list-bookmarks) ;; used to load the bookmarks and be able to use the keychord binding

;;************************************************************************************
;; Macro definition
;;************************************************************************************

;; Macro to align with ,
(fset 'pk_coma
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([134217848 97 108 105 103 110 tab 32 114 101 103 down down tab tab 44 return] 0 "%d")) arg)))

;; Macro to align with ;
(fset 'pk_semicol
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([134217848 97 108 105 103 110 32 114 101 103 101 return 59 return] 0 "%d")) arg)))

;; Macro to align with <
(fset 'pk_less
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([134217848 97 108 105 103 110 32 114 101 103 101 120 return 60 return] 0 "%d")) arg)))

;; Macro to align with =
(fset 'pk_equal
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([134217848 97 108 105 103 110 32 114 101 103 101 120 112 return 61 return] 0 "%d")) arg)))

;; Macro to align with (
(fset 'pk_open_parenthesis
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([134217848 97 108 105 103 110 32 114 101 120 101 112 backspace backspace backspace 103 101 120 112 return 40 return] 0 "%d")) arg)))

;; Macro to align with :
(fset 'pk_col
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([134217848 97 108 105 103 110 32 114 101 103 down down return 58 return] 0 "%d")) arg)))

;; Macro to align with < then ;
(fset 'pk_less_semi
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([134217848 97 108 105 103 110 32 114 101 103 101 120 return 60 return 24 24 134217848 97 108 105 103 110 32 114 101 103 101 120 112 return 59 return] 0 "%d")) arg)))

;; Macro to align with : then ;
(fset 'pk_col_semicol
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([134217848 97 108 105 103 110 32 114 101 103 down down return 58 return 24 24 134217848 97 108 105 103 110 32 114 101 103 down down tab tab 59 return] 0 "%d")) arg)))

;; Macro to align with : /
(fset 'pk_slash
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([134217848 97 108 103 backspace 105 103 110 down down down up up up up up return 47 return] 0 "%d")) arg)))

;;************************************************************************************
;; Function for mouse rectangle selection
;;************************************************************************************

(defun mouse-start-rectangle (start-event)
  (interactive "e")
  (deactivate-mark)
  (mouse-set-point start-event)
  (rectangle-mark-mode +1)
  (let ((drag-event))
    (track-mouse
      (while (progn(setq drag-event (read-event))
             (mouse-movement-p drag-event))
      (mouse-set-point drag-event)))))

(global-set-key (kbd "S-<down-mouse-1>") #'mouse-start-rectangle)



;;************************************************************************************
;; For arduino file extension mode
;;************************************************************************************

;;(add-to-list 'load-path "~/emacs/.emacs.d/elpa/arduino-mode")
;;(setq auto-mode-alist (cons '("\\.\\(pde\\|ino\\)$" . arduino-mode) auto-mode-alist))
;;(autoload 'arduino-mode "arduino-mode" "Arduino editing mode." t)

(add-to-list 'auto-mode-alist '("\\.ino\\'" . c-mode))


;;************************************************************************************
;; To slide one buffer to any direction without swaping
;;************************************************************************************

(defun slide-buffer (dir)
  "Move current buffer into window at direction DIR.
DIR is handled as by `windmove-other-window-loc'."
  (require 'windmove)
  (let ((buffer (current-buffer))
        (target (windmove-find-other-window dir)))
    (if (null target)
        (user-error "There is no window %s from here" dir)
      (switch-to-prev-buffer)
      (select-window target)
      (switch-to-buffer buffer nil t))))

(defun slide-buffer-up    () (interactive) (slide-buffer 'up))
(defun slide-buffer-down  () (interactive) (slide-buffer 'down))
(defun slide-buffer-left  () (interactive) (slide-buffer 'left))
(defun slide-buffer-right () (interactive) (slide-buffer 'right))
;;************************************************************************************
;; Margin modif in git log
;;************************************************************************************
;;(use-package magit-log
;;  :init
;;  (progn
;;    ;; (setq magit-log-margin '(t age magit-log-margin-width t 18)) ;Default value
;;    (setq magit-log-margin '(t age-abbreviated magit-log-margin-width :author 11)))
;;  :config
;;  (progn
;;    ;; Abbreviate author name. I added this so that I can view Magit log without
;;    ;; too much commit message truncation even on narrow screens (like on phone).
;;    (defun modi/magit-log--abbreviate-author (&rest args)
;;      "The first arg is AUTHOR, abbreviate it.
;;First Last  -> F Last
;;First.Last  -> F Last
;;Last, First -> F Last
;;First       -> First (no change).
;;
;;It is assumed that the author has only one or two names."
;;      ;; ARGS               -> '((REV AUTHOR DATE))
;;      ;; (car ARGS)         -> '(REV AUTHOR DATE)
;;      ;; (nth 1 (car ARGS)) -> AUTHOR
;;      (let* ((author (nth 1 (car args)))
;;             (author-abbr (if (string-match-p "," author)
;;                              ;; Last, First -> F Last
;;                              (replace-regexp-in-string "\\(.*?\\), *\\(.\\).*" "\\2 \\1" author)
;;                            ;; First Last -> F Last
;;                            (replace-regexp-in-string "\\(.\\).*?[. ]+\\(.*\\)" "\\1 \\2" author))))
;;        (setf (nth 1 (car args)) author-abbr))
;;      (car args))                       ;'(REV AUTHOR-ABBR DATE)
;;    (advice-add 'magit-log-format-margin :filter-args #'modi/magit-log--abbreviate-author)))
;;

;;*********************************************************
;; highlight
;;*********************************************************

(load "hl-anything.el")
(hl-highlight-mode 1)

;;*********************************************************
;; Replace selection on the fly
;;*********************************************************

(delete-selection-mode 1)
