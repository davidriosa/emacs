To install the Emacs configuration from gitlab, clone of git need to be done on the directory ~/emacs

To access gitlab with an ST computer, you need to install the program "GIT BASH" with the tools app.
once installed, run it and go to the path of your linux home.
use the following commands. In this example, my linux home was bind to the letter X

cd X:
We need to disable the SSL verification to be able to clone the repository type the following command:
  git config --global http.sslVerify false

run the following command to retreive the gitlab
git clone https://gitlab.com/davidriosa/emacs.git ./emacs

To set up the configuration files to run Emacs, it is necessary to perform the following commands:
mv .emacs .emacs_old
mv .emacs.d .emacs.d_old
ln -s ./emacs/.emacs_02112020 .emacs
ln -s ./emacs/.emacs.d .emacs.d

Lunch emacs and Enjoy !!!!

                     ---------
                 ---/         \---
               -/                 \-
              /                     \
             /     -----     -----   \
            /     (     )   (     )   \
           /       -----     -----     \
           |                           |
           |                           |
           |                           |
           \  --                   /-  /
            \   \-               //   /
             \    \--------------    /
              \                     /
               -\                 /-
                 ---\         /---
                     ---------
